<div>
     <h1>Crear Artículo</h1>
    
    <form wire:submit.prevent="save"> {{--  Este "save" Es un nombre cualquiera para la acción y debe estar disponible en la clase del componente --}}
       <label> 
            <input wire:model="article.title" type="text" placeholder="Título">
           

            @error('article.title') <div>{{ $message }}</div>@enderror
   
        </label>

        <label> 
            <input wire:model="article.slug" type="text" placeholder="Url amigable">
           

            @error('article.slug') <div>{{ $message }}</div>@enderror
   
        </label>

        <label>
        <textarea wire:model="article.content" placeholder="Contenido"></textarea>

           @error('article.content') <div>{{ $message }}</div>@enderror
      

        </label>

        
            <input type="submit" value="Guardar"> 
        
    </form>
</div>
